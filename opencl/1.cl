/******************************************************************************/
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   1.cl                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/12 14:28:30 by mstorcha          #+#    #+#             */
/*   Updated: 2018/06/12 14:28:33 by mstorcha         ###   ########.fr       */
/*                                                                            */
/******************************************************************************/

#include "rendering.h.cl"

int		return_color(t_color c)
{
	return ((int)c.r * 0x10000 + (int)c.g * 0x100 + (int)c.b);
}

/*
**		INTERSECTIONS
*/

float2 IntersectRayCone(float3 O, float3 D, t_figure cone)
{
	float A = cone.angle;
	float3 d = {cone.d.x, cone.d.y, cone.d.z};
	float3 p = {cone.p.x, cone.p.y, cone.p.z};

	float3 V = D;
	float3 P = O;
	float3 Pa = {cone.p.x, cone.p.y, cone.p.z};
	float3 Va = (d - p) / fast_length(d - p);
	float3 dP = P - Pa;

	float sc1 = dot(V, Va);
	float sc2 = dot(dP, Va);
	float3 tmp1 = V - Va * dot(V, Va);
	float3 tmp2 = dP - Va * dot(Va, dP);

	float A1 = cos(A) * cos(A) * dot(tmp1, tmp1)
					- sin(A) * sin(A)* sc1 * sc1;
	float B1 = 2.0 * cos(A) * cos(A) * dot(tmp1, tmp2)
					- 2.0 * sin(A) * sin(A) * sc1 * sc2;
	float C1 = cos(A) * cos(A) * dot(tmp2, tmp2)
					- sin(A) * sin(A)* sc2 * sc2;

	float desk = B1 * B1 - 4.0 * A1 * C1;
	if (desk < 0)
		return (float2){INFINITY, INFINITY};
	
	float t1 = (-B1 + sqrt(desk)) / (2.0 * A1);
	float t2 = (-B1 - sqrt(desk)) / (2.0 * A1);
	return (float2){t1, t2};
}

float2 IntersectRaySphere(float3 O, float3 D, t_figure sphere)
{
	float R = sphere.radius;
	float3 C = {sphere.p.x, sphere.p.y, sphere.p.z};
	float3 oc = (float3){O.x - C.x, O.y - C.y, O.z - C.z};

	float k1 = D.x * D.x + D.y * D.y + D.z * D.z;
	float k2 = 2 * (oc.x * D.x + oc.y * D.y + oc.z * D.z);
	float k3 = oc.x * oc.x + oc.y * oc.y + oc.z * oc.z - R * R;

	float desk = k2 * k2 - 4 * k1 * k3;
	if (desk < 0)
		return (float2){INFINITY, INFINITY};
	
	float t1 = (-k2 + sqrt(desk)) / (2 * k1);
	float t2 = (-k2 - sqrt(desk)) / (2 * k1);
	return (float2){t1, t2};
}

float2 IntersectRayCylinder(float3 O, float3 D, t_figure cyl)
{
	float R = cyl.radius;

	float3 V = D;
	float3 P = O;
	float3 P2 = {cyl.d.x, cyl.d.y, cyl.d.z};
	float3 P1 = {cyl.p.x, cyl.p.y, cyl.p.z};
	float3 Va = (P2 - P1) / fast_length(P2 - P1);	
	float3 dP = P - P1;

	float3 tmp1 = V - (Va * dot(V, Va));
	float3 tmp2 = dP - (Va * dot(Va, dP));
	float A = dot(tmp1, tmp1);
	float B = 2 * dot(tmp1, tmp2);
	float C = dot(tmp2, tmp2) - R * R;

	float desk = B * B - 4 * A * C;
	if (desk < 0)
		return (float2){INFINITY, INFINITY};
	
	float t1 = (-B + sqrt(desk)) / (2 * A);
	float t2 = (-B - sqrt(desk)) / (2 * A);
	return (float2){t1, t2};
}

float IntersectRayPlane(float3 O, float3 D, t_figure plane)
{
	float	t;
	float3	l_temp;
	float3 d = {plane.d.x, plane.d.y, plane.d.z};
	float3 p = {plane.p.x, plane.p.y, plane.p.z};

	l_temp = p - O;
	t = dot(l_temp, d);
	t = t / dot(D, d);
	return t;
}



/*
**




**
*/


float3		rotate_ort(float3 point, t_point rot)
{
	float3 od;
	float3 dv;
	float3 tr;
	float3 rot_rad;

	rot_rad.x = rot.x * M_PI / 180.0;
	rot_rad.y = rot.y * M_PI / 180.0;
	rot_rad.z = rot.z * M_PI / 180.0;
	od.x = point.x;
	od.y = point.y * cos(rot_rad.x) + point.z * sin(rot_rad.x);
	od.z = point.z * cos(rot_rad.x) - point.y * sin(rot_rad.x);
	dv.x = od.x * cos(rot_rad.y) - od.z * sin(rot_rad.y);
	dv.y = od.y;
	dv.z = od.z * cos(rot_rad.y) + od.x * sin(rot_rad.y);
	tr.x = dv.x * cos(rot_rad.z) + dv.y * sin(rot_rad.z);
	tr.y = dv.y * cos(rot_rad.z) - dv.x * sin(rot_rad.z);
	tr.z = dv.z;
	return tr;
}

t_closest		closest_fig(float3 O, float3 D,
			float min, float max, __global t_figure *figures, int o_n, int l_n)
{
	t_figure ret, figure;
	float closest = INFINITY;
	int i = -1;
	while(++i < o_n)
	{
		figure = figures[i];
		if (figure.type == CYLINDER)
		{
			float2 tmp = IntersectRayCylinder(O, D, figure);
			float t1 = tmp.x;
			float t2 = tmp.y;
			
			if (t1 >= min && t1 <= max && t1 < closest)
			{
				closest = t1;
				ret = figure;
			}
			if (t2 >= min && t2 <= max && t2 < closest)
			{
				closest = t2;
				ret = figure;
			}
			continue ;
		}
		else if (figure.type == CONE)
		{
			float2 tmp = IntersectRayCone(O, D, figure);
			float t1 = tmp.x;
			float t2 = tmp.y;
			
			if (t1 >= min && t1 <= max && t1 < closest)
			{
				closest = t1;
				ret = figure;
			}
			if (t2 >= min && t2 <= max && t2 < closest)
			{
				closest = t2;
				ret = figure;
			}
			continue ;
		}
		else if (figure.type == PLANE)
		{
			float t = IntersectRayPlane(O, D, figure);
		
			if (t >= min && t <= max && t < closest)
			{
				closest = t;
				ret = figure;
			}
			continue ;
		}
		else if (figure.type == SPHERE)
		{
			float2 tmp = IntersectRaySphere(O, D, figure);
			float t1 = tmp.x;
			float t2 = tmp.y;
			
			if (t1 >= min && t1 <= max && t1 < closest)
			{
				closest = t1;
				ret = figure;
			}
			if (t2 >= min && t2 <= max && t2 < closest)
			{
				closest = t2;
				ret = figure;
			}
			continue ;
		}
	}
	return (t_closest){closest, ret};
}

float	compute_light(float3 P, float3 N, float3 V, float s, __global t_figure *figures,
					__global t_figure *light, int o_n, int l_n)
{
	float koef = 0.2;
	int i = -1;
	while (++i < l_n)
	{
		t_figure l = light[i];
		float3 Lp = {l.p.x, l.p.y, l.p.z};
		float3 L = Lp - P;

		//shadow
		t_closest clos = closest_fig(P, L, 0.001, 0.99, figures, o_n, l_n);
		float closest = INFINITY;
		closest = clos.closest;
		if (closest != INFINITY)
			continue;

		//difuse
		float n_dot_l = N.x * L.x + N.y * L.y + N.z * L.z;
		if (n_dot_l > 0)
			koef += l.angle * n_dot_l / (fast_length(N) * fast_length(L));

		//zerk
		if (s != -1)
		{
			float3 R = N * 2 * n_dot_l - L;
			float r_dot_v = R.x * V.x + R.y * V.y + R.z * V.z;
			if (r_dot_v > 0)
				koef += l.angle*pow(r_dot_v / (fast_length(R) * fast_length(V)), s);
		}
	}
	return (koef > 1.0) ? 1.0 : koef;
}


t_color TraceRay(float3 O, float3 D, float min, float max, __global t_figure *figures,
					__global t_figure *light, int o_n, int l_n)
{
	float closest;
	t_figure figure;
	float c_l = 0;
	t_closest clos = closest_fig(O, D, min, max, figures, o_n, l_n);
	closest = clos.closest;
	if (closest == INFINITY)
		return (t_color){0, 0, 0};
	figure = clos.figure;

	
	float3 P = O + D * closest;
	if (figure.type == PLANE)
	{
		float3 d = {figure.d.x, figure.d.y, figure.d.z};
		float3 N;
		if (dot(D, d) < 0)
			N = (d / fast_length(d));
		else 
			N = (-d / fast_length(d));
		c_l = compute_light(P, N, -D, 10, figures, light, o_n, l_n);
	}
	else if (figure.type == CONE)
	{
		float3 d = {figure.d.x, figure.d.y, figure.d.z};
		float3 p = {figure.p.x, figure.p.y, figure.p.z};

		float3 T = d-p;
		T = T / fast_length(T);
		float3 N = P - p;
		T = T * dot(N, T);
		N = N - T;
		N = fast_normalize(N);

		c_l = compute_light(P, N, -D, 20, figures, light, o_n, l_n);
	}
	else if (figure.type == SPHERE)
	{
		float3 p = {figure.p.x, figure.p.y, figure.p.z};
		float3 N = P - p;
		N = N / fast_length(N);
		c_l = compute_light(P, N, -D, 10, figures, light, o_n, l_n);
	}
	else if (figure.type == CYLINDER)
	{
		float3 p = {figure.p.x, figure.p.y, figure.p.z};
		float3 d = {figure.d.x, figure.d.y, figure.d.z};
		float3 N = P - p;
		float3 T = d - p;
		T = T / fast_length(T);
		T = T * dot(N, T);
		N = (N - T) / fast_length(N - T);
		c_l = compute_light(P, N, -D, 50, figures, light, o_n, l_n);
	}
	c_l > 1.0F ? c_l = 1.0f : 0;
	return (t_color){figure.color.r * c_l,
						figure.color.g * c_l,
						figure.color.b * c_l};
}

__kernel void rendering(__global int * data, __global t_figure *figures,
					__global t_figure *light, t_figure cam,
					int l_n, int o_n)
{
	int j = get_global_id(0);
	int i = get_global_id(1);

	float d = 1; // vv from cam
	float3 O = {cam.p.x, cam.p.y, cam.p.z};
	float3 D;
	D.x = ((float)i - (float)WIDTH / 2) * 0.5 / (float)WIDTH;
	D.y = (-(float)j + (float)WIDTH / 2) * 0.5 / (float)HEIGHT;
	D.z = d;
	D = rotate_ort(D, cam.d);

	t_color c = TraceRay(O, D, 1.0F, INFINITY, figures, light, o_n, l_n);

	data[j * 1200 + i] = return_color(c);
}
