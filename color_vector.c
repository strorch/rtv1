/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/26 13:09:26 by mstorcha          #+#    #+#             */
/*   Updated: 2018/05/26 13:09:28 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_point		*norming(t_point *p)
{
	*p = num_dil(*p, v_length(*p));
	return (p);
}

t_point		num_dil(t_point p, float n)
{
	return (t_point){p.x / n, p.y / n, p.z / n};
}

float		v_length(t_point p)
{
	return (sqrt(p.x * p.x + p.y * p.y + p.z * p.z));
}

int			return_color(t_color c)
{
	return ((int)c.r * 0x10000 + (int)c.g * 0x100 + (int)c.b);
}
