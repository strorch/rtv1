camera { dir: -10,-10,0; pnt: 0,0,0; }

sphere { pnt: 0,2,2; rds: 10; clr: 255,255,0; }

cylinder { pnt: 0,0,0; dir: 0,0,0; clr: 255,0,0; rds: 1; }

sphere { pnt: -0.5,0,0; rds: 2; clr: 255,255,0; }

cylinder { pnt: 1,-0.5,5; dir: 4,2,5; clr: 255,0,0; rds: 1; }

cylinder { pnt: 1,-0.5,5; dir: 4,2,5; clr: 255,0,0; rds: 1; }

cone { pnt: 2,0,6; dir: 6,2,6; alp: 5; clr: 0,255,0; } 

plane { pnt: 0,-2,0; dir: 0.1,1,-0.3; clor: 255,155,0; }


sphere { pnt: -2,1,3; rds: 1; clr: 0,0,255; }

sphere { pnt: 2,1,3; rds: 1; clr: 0,255,0; }

sphere { pnt: 0,-5001,0; rds: 5000; clr: 255,255,0; }




sphere { pnt: -1,1,3; rds: 0.5; clr: 255,0,100; }


cylinder { pnt: 1,-0.5,5; dir: 4,2,5; clr: 255,0,0; rds: 0.2; }
sphere { pnt: -1,-1,5; rds: 0.5; clr: 0,100,0; }

plane { pnt: 5,-2,0; dir: 0.1,1,0; clr: 255,155,0; }
sphere { pnt: 1,-1,3; rds: 0.5; clr: 0,0,255; }

cone { pnt: 0,0,3; dir: 0,2,3; alp: 20; clr: 0,255,0; } 

light { pnt: 0,0,0; int: 0.8; }