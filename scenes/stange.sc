
plane { pnt: 5,-1,0; dir: 0,1,0; clr: 255,155,0; }
plane { pnt: 5,1,0; dir: 0,1,0; clr: 255,155,0; }
plane { pnt: 0,0,8; dir: 1,0,0; clr: 255,155,0; }

sphere { pnt: -1,-1,1; rds: 0.3; clr: 255,0,0 }
sphere { pnt: 1,-1,1; rds: 0.3; clr: 255,0,0 }

sphere { pnt: 1,1,1; rds: 0.3; clr: 255,0,0 }
sphere { pnt: -1,1,1; rds: 0.3; clr: 255,0,0 }

sphere { pnt: 1,-1,6; rds: 0.3; clr: 255,0,0 }
sphere { pnt: 1,1,6; rds: 0.3; clr: 255,0,0 }

sphere { pnt: -1,-1,6; rds: 0.3; clr: 255,0,0 }
sphere { pnt: -1,1,6; rds: 0.3; clr: 255,0,0 }

sphere { pnt: 0,-0.7,3; rds: 0.2; clr: 255,0,0 }

light { pnt: 1,0,0; int: 0.8; }

camera { dir: 0,0,0; pnt: 0,0,-6; }
