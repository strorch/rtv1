
plane { pnt: 5,-2,0; dir: 0.1,1,0; clr: 255,155,0; }

cone { pnt: 5,0.5,12; dir: 5,1,12; alp: 5; clr: 0,255,0; } 

cylinder { pnt: 0,0,5; dir: -1,2,5; rds: 0.5; clr: 255,0,255; }

sphere { pnt: -2,0,1; rds: 1; clr: 255,0,0 }


light { pnt: 0,0,0; int: 0.8; }

light { pnt: 3,3,3; int: 0.8; }

camera { dir: -10,-10,0; pnt: 0,-2,0; }
